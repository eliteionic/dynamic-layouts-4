import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	public stories: Object[] = [];

	constructor(private platform: Platform) {

		this.stories = [
			{title: 'Door Handles Mysteriously Vanishing Leaves Thousands Trapped in Rooms', author: 'Joshua Morony'},
			{title: 'Are Your Shorts Out to Get You?', author: 'Joshua Morony'},
			{title: 'IE8 Default Browser for New iPhone 9', author: 'Joshua Morony'},
			{title: 'The Secret Health Benefits of Muffins', author: 'Joshua Morony'},
			{title: 'Looting Begins as IE8 Riots Continue', author: 'Joshua Morony'},
			{title: 'Mysterious Object Eclipses Sun', author: 'Joshua Morony'},
			{title: 'Is Your Spouse Having an Affair with an AI?', author: 'Joshua Morony'},
			{title: 'Tourism Plummets as Virtual Holidays Gain Popularity', author: 'Joshua Morony'},
			{title: 'Does This Adelaide Man Have the Secret Formula for Winning the Lottery?', author: 'Joshua Morony'}
		];

	}

}