import { Component, ElementRef, Renderer2 } from '@angular/core';

import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private element: ElementRef,
    private renderer: Renderer2
  ) {
    this.initializeApp();
  }

  initializeApp() {

    if(this.platform.is('desktop')){
      this.renderer.addClass(this.element.nativeElement, 'desktop');
    }

  }
}